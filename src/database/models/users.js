'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
        email: DataTypes.STRING,
        username: DataTypes.STRING,
        password: DataTypes.STRING
    }, {
        timestamps: true,
        paranoid: true,
        hooks: {
            beforeCreate: (user) => {
                const salt = bcrypt.genSaltSync();
                user.password = bcrypt.hashSync(user.password, salt);
            }
        },
        instanceMethods: {
        }
    });
    users.associate = function(models) {
    // associations can be defined here
    };

    users.prototype.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    };

    users.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
      
        delete values.password;
        return values;
    };
    
    return users;
};