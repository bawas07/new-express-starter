const winston = require('winston');
const winston_daily = require('winston-daily-rotate-file');
const config = require('config');
const loggerPath = config.get('dirPath');

const tsFormat = () => (new Date()).toLocaleTimeString();

const logOpts = {
    transports: [
        new (winston.transports.Console)({colorize: true}),
        new winston.transports.File({
            filename: loggerPath.log_default_path,
            timestamp: tsFormat,
            handleExceptions: true,
            colorize: true,
            json: true,
            maxsize: 1024000, // 10MB
            maxFiles: 20,
            level: 'debug'
        }),
        new (winston_daily)({
            filename: loggerPath.log_info_path,
            timestamp: tsFormat,
            handleExceptions: false,
            colorize: true,
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 100,
            level: 'info',
            datePattern: 'DD-MM-YYYY',
            prepend: true,
            formatter: function (options) {
                // Return string will be passed to logger.
                return options.timestamp() + ' ' + options.level.toUpperCase() + ' ' + (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
            }
        })
    ],
    exceptionHandlers: [
        new (winston.transports.Console)({colorize: true}),
        new (winston.transports.File)({
            filename: loggerPath.log_exception_path,
            timestamp: tsFormat,
            handleExceptions: true,
            colorize: true,
            level: 'error',
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 10
        })
    ]
};
const logger = new (winston.Logger)(logOpts);

module.exports = logger;